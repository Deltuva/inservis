<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Minde',
            'email' => 'deltuva.mindaugas@gmail.com',
            'password' => bcrypt('1')
        ]);
    }
}
