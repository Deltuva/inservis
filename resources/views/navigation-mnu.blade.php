@if(!isset($innerLoop))
<ul class="navbar-nav">
@else
<ul class="dropdown-menu">
@endif

@php

    if (Voyager::translatable($items)) {
        $items = $items->load('translations');
    }

@endphp

@foreach ($items as $item)
    
    @php
    
        $originalItem = $item;
        if (Voyager::translatable($item)) {
            $item = $item->translate($options->locale);
        }

        $listItemClass = 'nav-item';
        $listItemLinkClass = 'nav-link';
        $linkAttributes =  null;
        $styles = null;
        $icon = null;
        $caret = null;

        // Background Color or Color
        if (isset($options->color) && $options->color == true) {
            $styles = 'color:'.$item->color;
        }
        if (isset($options->background) && $options->background == true) {
            $styles = 'background-color:'.$item->color;
        }

        if($originalItem) {
            $linkAttributes =  'class="nav-link dropdown-toggle"';
            $caret = '<span class="caret"></span>';

            if(url($originalItem->link()) == url()->current()){
                $listItemClass = 'nav-item dropdown active';
                $listItemId = "{$listItemClass}-{$item->id}";
            }else{
                $listItemClass = 'nav-item dropdown';
                $listItemId = "{$listItemClass}-{$item->id}";
            }
        }

        // With Children Attributes
        if(!$originalItem->children->isEmpty()) {
            $linkAttributes =  'class="nav-link dropdown-toggle"';
            $caret = '<span class="caret"></span>';

            if(url($item->link()) == url()->current()){
                $listItemId = "{$listItemClass}-{$item->id}";
                $listItemClass = 'nav-item dropdown active';
            }else{
                $listItemId = "{$listItemClass}-{$item->id}";
                $listItemClass = 'nav-item  dropdown meniu';
            }
        }

        // Set Icon
        if(isset($options->icon) && $options->icon == true){
            $icon = '<i class="' . $item->icon_class . '"></i>';
        }
        
    @endphp

    <li id="{{ $listItemId }}" class="{{ $listItemClass }}">
        <a href="{{ url($item->link()) }}" {!! $linkAttributes or '' !!} class="{{ $listItemLinkClass }}">
            {!! $icon !!}
                {{ $item->title }}
            {!! $caret !!}
        </a>
        @if(!$originalItem->children->isEmpty())
        @include('voyager::menu.bootstrap', ['items' => $originalItem->children, 'options' => $options, 'innerLoop' => true])
        @endif
    </li>
@endforeach

</ul>
